; This file describes the core project requirements for Oseds Video Several
; patches against Drupal core and their associated issue numbers have been
; included here for reference.

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = 7.23

; ------- installation profile --------

; Download the Oseds Video install profile
projects[osedsvideo][type] = profile
projects[osedsvideo][download][type] = git
projects[osedsvideo][download][url] = https://bitbucket.org/oseds/profile-video.git
;projects[osedsvideo][download][branch] = 7.x-1.3
